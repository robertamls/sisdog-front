import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import "./styles.css";
import { Slide } from "react-slideshow-image";
import "react-slideshow-image/dist/styles.css";
import image1 from "./images/katy.jpg";
import image2 from "./images/gaga.jpg";
import image3 from "./images/britney.jpg";

export class Animais extends Component {

  constructor(props) {
    super(props);

    this.slideRef = React.createRef();
      this.state = {
        current: 0
      };

    this.state = {
      id: 0,
      nome: '',
      raca: '',
      idade: '',
      genero: '',
      deficiencia: '',
      castrado: '',
      vacinado: '',
      tamanho: '',
      peso: '',
      cor: '',
      dataResgate: '',
      observacao: '',
      animais: [],
      modalAberta: false
    };

    this.buscarAnimais = this.buscarAnimais.bind(this);
    this.buscarAnimal = this.buscarAnimal.bind(this);
    this.inserirAnimal = this.inserirAnimal.bind(this);
    this.atualizarAnimal = this.atualizarAnimal.bind(this);
    this.excluirAnimal = this.excluirAnimal.bind(this);

    this.renderTabela = this.renderTabela.bind(this);
    this.abrirModalInserir = this.abrirModalInserir.bind(this);
    this.fecharModal = this.fecharModal.bind(this);

    this.atualizaNome = this.atualizaNome.bind(this);
    this.atualizaRaca = this.atualizaRaca.bind(this);
    this.atualizaIdade = this.atualizaIdade.bind(this);
    this.atualizaGenero = this.atualizaGenero.bind(this);
    this.atualizaDeficiencia = this.atualizaDeficiencia.bind(this);
    this.atualizaCastracao = this.atualizaCastracao.bind(this);
    this.atualizaVacina = this.atualizaVacina.bind(this);
    this.atualizaTamanho = this.atualizaTamanho.bind(this);
    this.atualizaPeso = this.atualizaPeso.bind(this);
    this.atualizaCor = this.atualizaCor.bind(this);
    this.atualizaData = this.atualizaData.bind(this);
    this.atualizaObservacao = this.atualizaObservacao.bind(this);
  }

  componentDidMount() {
    this.buscarAnimais();
  }

  // GET  ALL
  buscarAnimais() {
    fetch('https://localhost:44371/api/sisdog/animais')
      .then(response => response.json())
      .then(data => this.setState({ animais: data }));
  }

  //GET ESPECIFY
  buscarAnimal(id) {
    fetch('https://localhost:44371/api/sisdog/animais/' + id)
      .then(response => response.json())
      .then(data => this.setState(
        {
          id: data.id,
          nome: data.nome,
          raca: data.raca,
          idade: data.idade,
          genero: data.genero,
          deficiencia: data.deficiencia,
          castrado: data.castrado,
          vacinado: data.vacinado,
          tamanho: data.tamanho,
          peso: data.peso,
          cor: data.cor,
          dataResgate: data.dataResgate,
          observacao: data.observacao
        }));
  }

  inserirAnimal = (animal) => {
    fetch('https://localhost:44371/api/sisdog/animais', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(animal)
    }).then((resposta) => {

      if (resposta.ok) {
        this.buscarAnimais();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    }).catch(console.log);
  }

  atualizarAnimal(animal) {
    fetch('https://localhost:44371/api/sisdog/animais/' + animal.id, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(animal)
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarAnimais();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  excluirAnimal = (id) => {
    fetch('https://localhost:44371/api/sisdog/animais/' + id, {
      method: 'DELETE',
    }).then((resposta) => {
      if (resposta.ok) {
        this.buscarAnimais();
        this.fecharModal();
      } else {
        alert(JSON.stringify(resposta));
      }
    });
  }

  atualizaNome(e) {
    this.setState({
      nome: e.target.value
    });
  }

  atualizaRaca(e) {
    this.setState({
      raca: e.target.value
    });
  }

  atualizaIdade(e) {
    this.setState({
      idade: e.target.value
    });
  }

  atualizaGenero(e) {
    this.setState({
      genero: e.target.value
    });
  }

  atualizaDeficiencia(e) {
    this.setState({
      deficiencia: e.target.value
    });
  }

  atualizaCastracao(e) {
    this.setState({
      castrado: e.target.value
    });
  }

  atualizaVacina(e) {
    this.setState({
      vacinado: e.target.value
    });
  }

  atualizaTamanho(e) {
    this.setState({
      tamanho: e.target.value
    });
  }

  atualizaPeso(e) {
    this.setState({
      peso: e.target.value
    });
  }

  atualizaCor(e) {
    this.setState({
      cor: e.target.value
    });
  }

  atualizaData(e) {
    this.setState({
      dataResgate: e.target.value
    });
  }

  atualizaObservacao(e) {
    this.setState({
      observacao: e.target.value
    });
  }

  abrirModalInserir() {
    this.setState({
      modalAberta: true
    })
  }

  abrirModalAtualizar(id) {
    this.setState({
      id: id,
      modalAberta: true
    });

    this.buscarAnimal(id);
  }

  fecharModal() {
    this.setState({
      id: 0,
      nome: '',
      raca: '',
      idade: '',
      genero: '',
      deficiencia: '',
      castrado: '',
      vacinado: '',
      tamanho: '',
      peso: '',
      cor: '',
      dataResgate: '',
      observacao: '',
      modalAberta: false
    })
  }

  submit = () => {
    const animal = {
      id: this.state.id,
      nome: this.state.nome,
      raca: this.state.raca,
      idade: this.state.idade,
      genero: this.state.genero,
      deficiencia: this.state.deficiencia,
      castrado: this.state.castrado,
      vacinado: this.state.vacinado,
      tamanho: this.state.tamanho,
      peso: this.state.peso,
      cor: this.state.cor,
      dataResgate: this.state.dataResgate,
      observacao: this.state.observacao
    };

    if (this.state.id === 0) {
      this.inserirAnimal(animal);
    } else {
      this.atualizarAnimal(animal);
    }
  }

  renderModal() {
    return (
      <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
        <Modal.Header closeButton>
          <Modal.Title>Preencha os dados do animal</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form id="modalForm" onSubmit={this.submit}>
            <div class="row">
              <div className="col">
                <Form.Group>
                  <Form.Label>Nome</Form.Label>
                  <Form.Control type='text' value={this.state.nome} onChange={this.atualizaNome} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Raça</Form.Label>
                  <Form.Control type='text' value={this.state.raca} onChange={this.atualizaRaca} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Idade</Form.Label>
                  <Form.Control type='text' value={this.state.idade} onChange={this.atualizaIdade} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Genero</Form.Label>
                  <Form.Control value={this.state.genero} onChange={this.atualizaGenero} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Deficiência</Form.Label>
                  <Form.Control type='text' value={this.state.deficiencia} onChange={this.atualizaDeficiencia} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Castrado</Form.Label>
                  <Form.Control value={this.state.castrado} onChange={this.atualizaCastracao} />
                </Form.Group>
              </div>
              <div className="col">
                <Form.Group>
                  <Form.Label>Vacinado</Form.Label>
                  <Form.Control value={this.state.vacinado} onChange={this.atualizaVacina} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Tamanho</Form.Label>
                  <Form.Control value={this.state.tamanho} onChange={this.atualizaTamanho} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Peso</Form.Label>
                  <Form.Control type='number' value={this.state.peso} onChange={this.atualizaPeso} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Cor</Form.Label>
                  <Form.Control type='text' value={this.state.cor} onChange={this.atualizaCor} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Data de Resgate</Form.Label>
                  <Form.Control type='date' value={this.state.dataResgate} onChange={this.atualizaData} />
                </Form.Group>
                <Form.Group>
                  <Form.Label>Observação</Form.Label>
                  <Form.Control type='text' value={this.state.observacao} onChange={this.atualizaObservacao} />
                </Form.Group>
              </div>
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.fecharModal}>
            Cancelar
      </Button>
          <Button variant="primary" form="modalForm" type="submit">
            Confirmar
      </Button>
        </Modal.Footer>
      </Modal>
    );
  }


  renderTabela() {
    return (
      <div className="container" id="divtabela">
        <br />
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Nome</th>
              <th>Raça</th>
              <th>idade</th>
              <th>Gênero</th>
              <th>Deficiência</th>
              <th>Castrado</th>
              <th>Vacinas em dia</th>
              <th>Tamanho</th>
              <th>Peso</th>
              <th>Cor</th>
              <th>Data Resgate</th>
              <th>Observação</th>
              <th>Opções</th>
            </tr>
          </thead>
          <tbody>
            {this.state.animais.map((animal) => (
              <tr key={animal.id}>
                <td>{animal.nome}</td>
                <td>{animal.raca}</td>
                <td>{animal.idade}</td>
                <td>{animal.genero}</td>
                <td>{animal.deficiencia}</td>
                <td>{animal.castrado}</td>
                <td>{animal.vacinado}</td>
                <td>{animal.tamanho}</td>
                <td>{animal.peso}</td>
                <td>{animal.cor}</td>
                <td>{animal.dataResgate}</td>
                <td>{animal.observacao}</td>
                <td>
                  <div>
                    <Button variant="link" onClick={() => this.abrirModalAtualizar(animal.id)}>Atualizar</Button>
                    <Button variant="link" onClick={() => this.excluirAnimal(animal.id)}>Excluir</Button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }

render() {
  const properties = {
    duration: 5000,
    autoplay: false,
    transitionDuration: 500,
    arrows: false,
    infinite: true,
    easing: "ease",
    indicators: (i) => <div className="indicator">{i + 1}</div>
  };
  const slideImages = [image1, image2, image3];

  return (
    <div className="container">
      <br />
      <h4>Animais registrados aguardando adoção</h4>
      <br />
        <div className="slide-container">
          <Slide ref={this.slideRef} {...properties}>
            {slideImages.map((each, index) => (
              <div key={index} className="each-slide">
                <img className="lazy" src={each} alt="sample" />
              </div>
            ))}
          </Slide>
        </div>
      <Button variant="primary" className="btn btn-info" onClick={this.abrirModalInserir}>Adicionar Animal</Button>
      {this.renderTabela()}
      {this.renderModal()}
    </div>
  );
}

}
