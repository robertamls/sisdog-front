import logo from './dog.gif';
import './App.css';

import React, { Component } from 'react';

export class Home extends Component {
  render() {
    return (
        <div className="App">
          <header className="App-header">
          <h1>Seja bem-vindo ao SisDog!</h1>
            <img src={logo}/>
            <p>Adote um doguinho e seja feliz!</p>
            </header>
        </div>
      );
  }
}
