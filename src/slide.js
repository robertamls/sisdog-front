import React from "react";
import { Zoom } from "react-slideshow-image";
import image1 from "./images/britney.jpg";
import image2 from "./images/gaga.jpg";
import image3 from "./images/katy.jpg";
// style was imported in index.css
// import "react-slideshow-image/dist/styles.css";

const images = [image1, image2, image3];

const zoomOutProperties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  scale: 0.4,
  arrows: true
};

const Slideshow = () => {
  return (
    <div className="slide-container">
      <Zoom {...zoomOutProperties}>
        {images.map((each, index) => (
          <img key={index} style={{ width: "100%" }} src={each} />
        ))}
      </Zoom>
    </div>
  );
};

function Slides() {
  return (
    <div className="Slides">
      <Slideshow/>
    </div>
  );
}

export default Slides;
